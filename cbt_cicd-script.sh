
######################################################################
#  This .sh file demonstrates how to create or update an automation task in Zephyr for Jira Cloud, run this task, and publish test results to Zephyr.
#  Author: SmartBear Software
######################################################################

######################################################################
#  Zephyr base URL.
#  DON'T CHANGE THE CONSTANT BELOW. KEEP IT AS IT IS..
######################################################################
zephyrBaseUrl="https://prod-api.zephyr4jiracloud.com/connect"

#######################################################################
#  Access and secret keys, and user id needed for connection to Zephyr for Jira. 
#  Replace the constants below with values relevant to your project and account.
#######################################################################

# The accessKey and secretKey to access your project. You can find them in your Jira project: Zephyr > API Keys.
accessKey="MzFkZTA1ZTgtMWRkZi0zNDY2LTk3OWUtYzg4OGQzMzhlNmI3IDVkODBmYTFmMzA5MjZkMGMzM2IyNDY3ZiBVU0VSX0RFRkFVTFRfTkFNRQ"
secretKey="b7ImCootkVUEEm76OP-B9gu1KJ1YSVEoc0gE64XbFoo"
# Id of the user who will create the automation task. You can find it in Jira.
accountId="5d80fa1f30926d0c33b2467f"   

#######################################################################
#  Create a JSON Web Token  (required to access Zephyr for Jira).
#  Keep this section as it is.
#######################################################################
echo "Generating a JSOM Web Token ... \n"
curl -o headers -s -d '{  "accessKey": "'"$accessKey"'"  , "secretKey": "'"$secretKey"'" ,"accountId": "'"$accountId"'","zephyrBaseUrl": "'"$zephyrBaseUrl"'","expirationTime":360000}' -H "Content-Type: application/json" -XPOST https://prod-vortexapi.zephyr4jiracloud.com/api/v1/jwt/generate
jwt="$(cat  headers | head -n 1)"
echo "The generated token: \n"
echo $jwt

#######################################################################
#  Define properties of the automation task.
#  Replace the values below with data relevant to your project.
#######################################################################
# Let's keep creating new timestamped cycles per CI run 
# Task info
taskName="cbt_z4j"                     
taskDescription="sending results from cbt"
automationFramework="JUNIT"  
projectKey="KIM"
versionName="test"

# Cycle info
cycleName="CBT Automation"
createNewCycle="true"
appendDateTimeInCycleName="true"

# Folder info
folderName="CBT pytest folder"
createNewFolder="true"
appendDateTimeInFolderName="true"
assigneeUser="5d80fa1f30926d0c33b2467f"

# Name of the test result file
# KEEP THIS AS IT IS - REFLECTS THE gitlab-ci.yml file's environment variables
resultPath=@$resultPath

#######################################################################
#  Create an automation task, run it, send test results to Zephyr.
#  Keep this section as it is.
#######################################################################
echo "Creating and running an automation task ..."
curl -o headers -s -v -H "accessKey: $accessKey" -H "jwt: $jwt" -H "Content-Type: multipart/form-data" -H "Content-Type: application/json" -F "jobName=$taskName" -F "jobDescription=$taskDescription" -F "automationFramework=$automationFramework" -F "projectKey=$projectKey" -F "versionName=$versionName" -F "cycleName=$cycleName" -F "createNewCycle=$createNewCycle" -F "appendDateTimeInCycleName=$appendDateTimeInCycleName" -F "folderName=$folderName" -F "createNewFolder=$createNewFolder" -F "appendDateTimeInFolderName=$appendDateTimeInFolderName" -F "assigneeUser=$assigneeUser" -F "file=$resultPath" -XPOST https://prod-vortexapi.zephyr4jiracloud.com/api/v1/automation/job/saveAndExecute
result="$(cat  headers | head -n 1)"
echo "Test results: \n"
echo $result
# END of the "Create task" code

#######################################################################
#  Update the properties of automation task and  run it, send test results to Zephyr.
#  
#  The code below uses "task properties" values defined in lines 40-58.
#  Update them to change task properties.
#
#  To run the code below, first comment out the code lines 64-68 that create your automation task, 
#  and then uncomment in the lines below.
#######################################################################

# Id of your automation task. You get it after the task is created.
#taskId="30E7566F0AE85E9478304F697A41D85789C12259AD19749FFE368DD8D77609CB"

#echo "Update and run the automation task... "

#curl -o headers -s -v -H "accessKey: $accessKey" -H "jwt: $jwt" -H "Content-Type: multipart/form-data" -H "Content-Type: application/json" -F "jobId=$taskId" -F "jobName=$taskName" -F "jobDescription=$taskDescription" -F "automationFramework=$automationFramework" -F "versionName=$versionName" -F "cycleName=$cycleName" -F "createNewCycle=$createNewCycle" -F "appendDateTimeInCycleName=$appendDateTimeInCycleName" -F "folderName=$folderName" -F "createNewFolder=$createNewFolder" -F "appendDateTimeInFolderName=$appendDateTimeInFolderName" -F "assigneeUser=$assigneeUser" -F "file=$resultPath" -XPUT https://prod-vortexapi.zephyr4jiracloud.com/api/v1/automation/job/updateAndExecute

#result="$(cat  headers | head -n 1)"
#echo $result

# END of the "Update task" code
