# CrossBrowserTesting, BitBar, and Zephyr for Jira

In this repository, we will highlight an example of integrating test management within Jira, with automated tests leveraging CBT and BitBar as the remote device cloud/farm solutions, running through GitLab CI.

## CrossBrowserTesting

Ditch your VMs and Device Lab. Easily run Manual, Visual, and Selenium Tests in the cloud
on 2050+ real desktop and mobile browsers. [CrossBrowserTesting](https://crossbrowsertesting.com/)

## BitBar

All-in-One Real Device Testing Cloud : Whether you want to do automated testing in any framework, manual app testing, or AI-driven codeless testing against real devices at scale, Bitbar has you covered. [BitBar](https://bitbar.com/)

## Zephyr For Jira

The #1 Agile Test Management Solution in Jira, perfect for teams focusing on Test Design, Execution, and Test Automation. [Zephyr for Jira](https://marketplace.atlassian.com/apps/1014681/zephyr-for-jira-test-management?hosting=cloud&tab=overview)	

## Workflow

In the examples shown in the repository, we will see two python scripts *bitbar_pytest.py* and *cbt_pytest.py*, which execute two test cases each against real devices and browsers in the cloud leveraging both CBT and BitBar. In this example, we will leverage GitLab as our CI framework of choice, and you can see the sample *gitlab-ci.yml* file included within the repostiory to see the syntax and structure of the sample pipeline itself.  

1. Fork/Clone this repo somewhere
2. Download/register/start your [GitLab Runner](https://docs.gitlab.com/runner/install/docker.html) - I am opting to run mine in a container (tagged as "docker2"), but you can choose to run it locally on your own machine (you might need to use the .bat files instead, and make some syntactual changes to the environment variables to the yml file)
3. Modify the Jira authentication/configuration information within the two *cicd-script.sh* files (explanation behind the steps within the sh files are included [here](https://support.smartbear.com/zephyr-for-jira-cloud/docs/continuous-integration/integration-script.html))
4. Modify the device/browser capabililties within the *gitlab-ci.yml* file if you'd like. Also remember to change the "tag" value to point to the gitlab-runner you installed/registered.
5. Watch it all come together - Automated, remote cloud executions with results getting reported back to Zephyr for Jira.

## Details & Explanations

### Shell Scripts explained
Within the shell scripts provided, you will see a section that reads:

```shell
# The accessKey and secretKey to access your project. You can find them in your Jira project: Zephyr > API Keys.
accessKey="YOUR JIRA ACCESS KEY"
secretKey="YOUR JIRA SECRET KEY"
# Id of the user who will create the automation task. You can find it in Jira.
accountId="JIRA ACCOUNT ID NUMBER"   
```
Follow the linked documentation from step 3 of the "workflow" section above to find/fill those out.

Still within the shell scripts, you will also see: 

```shell
# Task info
taskName="cbt_z4j"                     
taskDescription="sending results from cbt"
automationFramework="JUNIT"  
projectKey="KIM"
versionName="test"

# Cycle info
cycleName="CBT Automation"
createNewCycle="true"
appendDateTimeInCycleName="true"

# Folder info
folderName="CBT pytest folder"
createNewFolder="true"
appendDateTimeInFolderName="true"
assigneeUser="DESIRED JIRA ACCOUNT ID NUMBER"

# Name of the test result file
resultPath=@$resultPath
```

Fill out the Jira Fields with the information that you woud like to populate. Keep the "resultPath" variable as it is, since it is referring to the dynamic environment variable that we will set later on within the *gitlab-ci.yml* file.

*Note: if your Jira instance/project has any additonal Jira fields that have been toggled to be mandatory (outside of normal default field configurations), you will need to add those fields into the curl command we are making here*

Concretely, we are simply making curl commands to create/update/execute the "Test Automation" jobs within our Zephyr for Jira instance (on the go through the CI framework). I am electing to use a shell script here as provided by the Zephyr for Jira documentation, but feel free to refer to the api docs, and create your own scripts (like Steve did [here](https://github.com/stevenjc/Selenium-Zephyr-CrossBrowserTesting-example/blob/master/sync_z4j.py) in Python)

### Both of the Python Scripts explained

There are two .py (test) scripts within the repository: "cbt_pytest.py" and "bitbar_pytest.py". These are both sample scripts provided by the respective solutions on their github pages, which have been modified very slightly to fit our needs. As an end user testing this workflow, you do not need to make any changes to either scripts. The only thing worth pointing out in these scripts are:

```python
class SeleniumCBT(unittest.TestCase):
    def setUp(self):
        #this information is provided via environment variables within
        #the gitlab-ci.yml file

        self.username = os.environ['CBT_USERNAME']
        self.authkey  = os.environ['CBT_APIKEY']
       
        self.api_session = requests.Session()
        self.api_session.auth = (self.username,self.authkey)
        self.test_result = None

        caps = {}
        caps['name'] = os.environ['CBT_BUILD_NAME']
        caps['build'] = os.environ['CBT_BUILD_NUMBER']
        caps['browserName'] = os.environ['CBT_BROWSER']

```
and

```python
class BitbarIOS(unittest.TestCase):
    def setUp(self):
        #this information is provided via environment variables within
        #the gitlab-ci.yml file

        bitbar_url = os.environ.get('BITBAR_URL') or "https://cloud.bitbar.com"
        appium_url = os.environ.get('BITBAR_APPIUM_URL') or 'https://appium.bitbar.com/wd/hub'
        
```
Where we are referring back to the environment variables set within our *gitlab-ci.yml* files to dictate which browser/device we'd like to test against. 

### gitlab-ci.yml file explained

The "build" stage job actually does nothing, so skipping over that.

Our first "test" stage job is going to execute our CBT test case. Here, we can see that we are using a python:3.6 image, and that we are using a tag-based execution (so that I know which containerized gitlab-runner I am using). 

```yaml
CBT_test-job:
  stage: test
  image: python:3.6
  tags: 
    - YOUR_GITLAB_RUNNER_TAG
```
Additionally we also see:

```yaml
variables:
    CBT_USERNAME: "YOUR_CBT_ACCOUNT@YOUR_ORG.com"
    CBT_APIKEY: "YOUR_AUTH_KEY"
    CBT_BUILD_NAME: "$CI_PROJECT_NAME"
    CBT_BUILD_NUMBER: "$CI_JOB_ID"
    CBT_BROWSER: "Safari"
    CBT_BROWSER_VERSION: "13"
    CBT_OPERATING_SYSTEM: "Mac OSX 10.15"
    CBT_RESOLUTION: "1366x768"
    resultPath: "$CI_PROJECT_DIR/cbt-reports/results.xml"
```

Where we are defining both the CBT authentication information as well as our desired browser capabilities. For both CBT and BitBar, please use the capabilities configurator within the respective tools to obtain these values. The only thing here is to just keep the "resultPath" variable as it is, since it is referring to the xml report to be generated by our script step below

```shell 
py.test --junitxml cbt-reports/results.xml cbt_pytest.py
```
As you might have guessed, this resultPath environment variable is also referenced within the shell scripts (which are used to push the results to Zephyr for Jira after our test execution)

```yaml
script:
    - apt-get update -qy
    - pip install -r requirements.txt
    - echo "We're using pytest with selenium"
    - py.test --junitxml cbt-reports/results.xml cbt_pytest.py
```
For the actual scripts of the "test" stage itself, we see that 
1. Update the package lists and the dependencies of our container
2. Get required python modules outlined in the requirements.txt file
3. Execute via py.test command, generating that xml report to the designated directory

After the test execution, we see that:

```yaml
after_script:
    - echo "pushing results to Z4J"
    - chmod +x cbt_cicd-script.sh 
    - ./cbt_cicd-script.sh
```
where we trigger the provided shell script to push the execution results back to Zephyr for Jira.

The exact same steps/logic applies to the other "test" stage job, which runs the BitBar test cases against the mobile devices in the cloud. Make sure to change the environment variables with your authentication info as well as any desired capabililties that you would like to change.

#### tangentially related notes:

If you do not see the mulitple "test" stages getting kicked off in parallel, you may need to redefine the maximum number of concurrencies possible by your gitlab runner. This can be done by modifying the config.toml file placed within your gitlab-runner installation directory (whether that be locally installed or mounted within your container volume). Mine looks something like this:

```yaml
concurrent = 2
check_interval = 0

[session_server]
  session_timeout = 1800
[[runners]]
  name = "SOME_DESCRIPTIVE_RUNNER_TAG"
  url = "https://gitlab.com/"
  token = "SOME_TOKEN"
  executor = "docker"
```

## Expected Results

Pipeline View

![image](/uploads/12f62c048a914f9930c1e084ea880a0c/image.png)

Sample Test Stage Job
![image](/uploads/420edc5df24b3d2358066e6568c45c1d/image.png)

Test Stage Failure

![image](/uploads/06bd37741f0ddc7f36e6d631e4928da3/image.png)

Push Results to Zephyr for Jira

![image](/uploads/3ccc14fb94181a25f03c70fa27606a48/image.png)
![image](/uploads/2e94317c2a37df55dabbe5d604beb10f/image.png)

Zephyr for Jira view
![image](/uploads/bc6912803dca5aeef47d08cb11f86ab4/image.png)

CBT view
![image](/uploads/bbfea47b28e439153e7e222912a57d25/image.png)

BitBar view
![image](/uploads/66b09734a057f5d2d44fdf7f76fdeb87/image.png)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.
